
load("szhang68_tree.rb")
load("szhang68_enum.rb")

#use array to build a tree
def buildTree(a)
	tree = P2Tree.new
	a.size.times do |i|
		tree.insertNode(a[i])
	end
	return tree
end

#test p2all?
def test_p2all?
	a = [23,12,4,8,5,9,45,2]
	tree = buildTree(a)
	r = tree.p2all?{|e| e > 50}
	raise "#{__method__} error!" if r
    p "#{__method__} passed!"
end
test_p2all?


#test p2any?
def test_p2any?
	a = [23,1,9,5,21,23,14]
	tree = buildTree(a)
	r = tree.p2any?{|e| e < 20}
	raise "#{__method__} error!" if !r
	p "#{__method__} passed!"
end
test_p2any?

#test collect
def test_p2collect
	a = [6,3,14,34,7,11,2]
	tree = buildTree(a)
	r = tree.p2collect{|e| e*2}
	raise "#{__method__} error!"if r != [12, 28, 6, 68, 14, 22, 4]
	p "#{__method__} passed!"

end
test_p2collect

#test collect_concat
def test_p2collect_concat
	a = [15,10,17,19,6,9,7]
	tree = buildTree(a)

	r = tree.p2collect_concat{|e| [e, -e]} 
	raise "#{__method__} error!"if r != [15, -15, 17, -17, 10, -10, 19, -19, 6, -6, 9, -9, 7, -7]
	p "#{__method__} passed!"

end
test_p2collect_concat


#test count
def test_p2count
	a = [14,21,3,6,18,22,5]
	tree = buildTree(a)

	r = tree.p2count{|e| e > 20}
	raise "#{__method__} error!"if r != 2
	p "#{__method__} passed!"

end
test_p2count


#test p2cycle
def test_p2cycle
	a = [9,2,6,12]
	tree = buildTree(a)

	r = []
	tree.p2cycle(2){|e| r << e}
	raise "#{__method__} error!" if r != [9, 12, 2, 6, 9, 12, 2, 6]
	p "#{__method__} passed!"
end
test_p2cycle


#test p2detect
def test_p2detect
	a = [3,8,2,11,4,6]
	tree = buildTree(a)

	r = tree.p2detect{|e| e%2 == 0 && e%3 == 0}
	raise "#{__method__} error!" if r != 6
	p "#{__method__} passed!"
end
test_p2detect


#test p2drop
def test_p2drop
	a = [18,2,3,4,7,6,11]
	tree = buildTree(a)

	r = tree.p2drop(3)
	raise "#{__method__} error!" if r != [4, 7, 11, 6]
	p "#{__method__} passed!"
	
	
end
test_p2drop

#test p2drop_while
def test_p2drpo_while
	a = [5,2,7,6,10,11]
	tree = buildTree(a)

	r = tree.p2drop_while{|e| e%5 == 0}
	raise "#{__method__} error!" if r != [7, 2, 6, 11]
	p "#{__method__} passed!"
	
end
test_p2drpo_while

#test p2each_cons
def test_p2each_cons
	a = [5,2,7,6,10,11]
	tree = buildTree(a)

	r = []
	tree.p2each_cons(4){|e| r << e}
	raise "#{__method__} error!" if r != [[5, 7, 2, 10], [7, 2, 10, 6], [2, 10, 6, 11]]
	p "#{__method__} passed!"
	
end
test_p2each_cons

#test p2each_slice
def test_p2each_slice

	a = [4,9,3,2,22,32,17,9,6,1]
	tree = buildTree(a)

	r = []
	tree.p2each_slice(4){|e| r << e}
	raise "#{__method__} error!" if r != [[4, 9, 3, 22], [2, 32, 1, 17], [9, 6]]
	p "#{__method__} passed!"
end
test_p2each_slice


#test p2each_with_index
def test_p2each_with_index
	a = [5,7,23,1,7,6,8]
	tree = buildTree(a)

	hash = {}
	tree.p2each_with_index{|item, index| hash[index] = item}
	raise "#{__method__} error!" if hash != {0=>5, 1=>7, 2=>1, 3=>23, 4=>6, 5=>7, 6=>8}
	p "#{__method__} passed!"

end
test_p2each_with_index

#test p2entries
def test_p2entries
	a = [8,3,21,4,17,12,5]
	tree = buildTree(a)

	r = tree.p2entries
	raise "#{__method__} error!" if r != [8, 21, 3, 4, 17, 12, 5]
	p "#{__method__} passed!"
	
end
test_p2entries

#test p2find
def test_p2find
	a = [14,21,3,6,18,22,5]
	tree = buildTree(a)

	r = tree.p2find{|e| e > 21}
	raise "#{__method__} error!" if r != 22
	p "#{__method__} passed!"

end
test_p2find


#test p2find_all
def test_p2find_all
	a = [23,1,9,5,21,23,14]
	tree = buildTree(a)

	r = tree.p2find_all{|e| e.even? }
	raise "#{__method__} error!" if r != [14]
	p "#{__method__} passed!"

	
end
test_p2find_all

#test p2find_index
def test_p2find_index
	a = [14,21,3,6,18,22,5]
	tree = buildTree(a)

	r = tree.p2find_index{|e| e == 20}
	raise "#{__method__} error!" if r != nil
	p "#{__method__} passed!"

end
test_p2find_index

#test p2first
def test_p2first
	a = [4,36,13,17,9,54,35]
	tree = buildTree(a)

	r = tree.p2first(4)
	raise "#{__method__} error!" if r != [4, 36, 54, 13]
	p "#{__method__} passed!"

end
test_p2first

#test p2group_by
def test_p2group_by
	a = [14,21,3,6,18,22,5]
	tree = buildTree(a)

	r = tree.p2group_by{|e| e%4}
	raise "#{__method__} error!" if r != {2=>[14, 22, 6, 18], 1=>[21, 5], 3=>[3]}
	p "#{__method__} passed!"

end
test_p2group_by

#test p2inject()
def test_p2inject
	a = [8,12,17,2,4,5,1]
	tree = buildTree(a)

	n = tree.p2inject(1){|s, e| s*e}
	raise "#{__method__} error!" if n != 65280
	p "#{__method__} passed!"
	
end
test_p2inject

#test para0inject()
def test_para0inject
	a = [8,12,17,2,4,5,1]
	tree = buildTree(a)

	m = tree.para0inject{|s, e| s+e}
	raise "#{__method__} error!" if m != 49 
	p "#{__method__} passed!"
	
end
test_para0inject

#test p2minmax
def test_p2minmax
	a = [5,2,7,6,10,11]
	tree = buildTree(a)

	r = tree.p2minmax{|a, b| a <=> b}
	raise "#{__method__} error!" if r != [2,11]
	p "#{__method__} passed!"
end
test_p2minmax

#test p2minmax_by
def test_p2minmax_by
	a = [4,9,22,32,17,9,6,1]
	tree = buildTree(a)

	r = tree.p2minmax_by{|e| e}
	raise "#{__method__} error!" if r != [1,32]
	p "#{__method__} passed!"
end
test_p2minmax_by


#test p2partition
def test_p2partition
	a = [3,8,2,11,4,6]
	tree = buildTree(a)

	r = tree.p2partition{|e| e%3 == 0}
	raise "#{__method__} error!" if r != [[3, 6], [8, 2, 11, 4]]
	p "#{__method__} passed!"
	
end
test_p2partition

#test p2reject
def test_p2reject
	a = [2,8,4,15,11,14,7]
	tree = buildTree(a)

	r = tree.p2reject{|e| e%2 != 0}
	raise "#{__method__} error!" if r != [2, 8, 4, 14]
	p "#{__method__} passed!"
	
end
test_p2reject

#test p2take
def test_p2take
	a = [23,1,9,5,21,23,14]
	tree = buildTree(a)

	r = tree.p2take(5)
	raise "#{__method__} error!" if r != [23, 1, 9, 21, 5]
	p "#{__method__} passed!"
	
end
test_p2take

#test p2take_while
def test_p2take_while
	a = [23,12,4,8,5,9,45,2]
	tree = buildTree(a)

	r = tree.p2take_while{|e| e >15}
	raise "#{__method__} error!" if r != [23, 45]
	p "#{__method__} passed!"
end
test_p2take_while

#test p2to_a
def test_p2to_a
	a = [4,36,13,17,9,54,35]
	tree = buildTree(a)

	r = tree.p2to_a
	raise "#{__method__} error!" if r != [4, 36, 54, 13, 35, 17, 9]
	p "#{__method__} passed!"
end
test_p2to_a

#test p2to_h
def test_p2to_h
	a = [5,2,7,6,10,11]
	tree = buildTree(a)

	r = tree.each_with_index.p2to_h
	raise "#{__method__} error!" if r != {5=>0, 7=>1, 2=>2, 10=>3, 6=>4, 11=>5}
	p "#{__method__} passed!"
end
test_p2to_h


