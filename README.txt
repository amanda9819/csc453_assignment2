+++++++++++++++++++++++++++++++++++++++++++++++++++++++++


1.	Name: Shuwen Zhang
	Course: CSC 453


++++++++++++++++++++++++++++++++++++++++++++++++++++++++


2.There are five files: szhang68_enum.rb, szhang68_enum_tests.rb, szhang68_tree.rb, szhang68_tree_tests.rb, and szhang68_tree_enum_tests.rb. The five files need to be stored at one folder. 

Then use "ruby szhang68_enum_tests.rb" at terminal to check the implementation of the iterator methods. Each unit test has two test case. One is array the other is hash. While the result is correct, the output of program is "*** array passed!" and "*** Hash passed!" where the "***" represents the name of function. If the iterator did not pass the test case, the output of program is "*** array error!" or "*** erray error!".

I implemented the simple binary tree in class named P2Tree. The data structure of the tree is stored in an array. If the value of new node is larger than present one and the left child of present node is nil, insert the new node as the left child, or if the value of new node is less than present one and the right child of present node is nil, insert the now node as the right child. Go through the whole tree and find the right place to insert the new node. 

In szhang68_tree.rb, the function of "insertNode" method is inserting new node. I implement a "buildTree" method here then call it when build the tree with the node array. Use "ruby szhang68_tree_test.rb" to test tree module. The results of tests are showned at terminal.

Use "ruby szhang68_tree_enum_test.rb" to implement the unit test for P2Tree and P2Enumerable integration. The results of tests are showned at terminal and the meaning of those results are same as the test of iterator methods mentioned before.

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++

3. The binary tree is implemented in an array. So campare the size of array with the times of iteration. When the times of iteration is same as the size of array, we can stop the iteration.


+++++++++++++++++++++++++++++++++++++++++++++++++++++++++

4.No
