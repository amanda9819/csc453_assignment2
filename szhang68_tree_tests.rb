load("szhang68_tree.rb")
load("szhang68_enum.rb")


#test insertNode
def test_insertNode
	a = [2,8,4,15,11,14,7]
	tree = P2Tree.new
	a.size.times do |i|
		tree.insertNode(a[i])
	end
	r = []
	tree.p2each{|e| r << e }
	raise "#{__method__} error!" if r != [2, 8, 15, 4, 11, 14, 7]
    p "#{__method__} passed!"
end
test_insertNode


#use array to build a tree
def buildTree(a)
	tree = P2Tree.new
	a.size.times do |i|
		tree.insertNode(a[i])
	end
	return tree
end

#test p2each
def test_p2each
	a = [24,13,20,45]
	tree = buildTree(a)
	r = []
	tree.p2each{|e| r << e} 
	raise "#{__method__} error!" if r != [24, 45, 13, 20]
    p "#{__method__} passed!"


end
test_p2each

#test p2each_with_level
def p2each_with_level
	a = [8,2,4,5,1]
	tree = buildTree(a)
	r = {}
	tree.p2each_with_level do|e,m| 
		r[e] = m
	end	
	raise "#{__method__} error!" if r != {8=>0, 2=>1, 4=>2, 1=>2, 5=>3}
    p "#{__method__} passed!"
	
end
p2each_with_level
