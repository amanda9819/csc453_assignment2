#This is the P2Enumerable


module P2Enumerable

# define all?
    def p2all?
        res = false
        p2each do |e|
            if res == true
                return res
            else
                res = yield(e)
            end
        end
        return res    
    end


    #define any?
    def p2any?
    	res = false
    	p2each do |e|
    		res = yield(e)
    	end
    	return res	
    end

    #define collect
    def p2collect
    	r = Array.new
    	p2each do |e|
    		r << yield(e)
            if r.class != Array
                r = [r]
            end
    	end
    	#p r
    	return r    	
    end

    #define collect_concat
 	def p2collect_concat
 		r = Array.new
 		n = Array.new
 		p2each do |e|
 			r = yield(e)
 			if r.class != Array
 				r = [r]
 			end
 			n = n + r
 		end
 		#p n
 		return n
 	end

	#define count
 	def p2count
 		i = 0
 		p2each do |e|
 			if yield(e)
 				i += 1
 			end
 		end
 		return i
 	end


 	#define cycle
 	def p2cycle(n = nil)
 		if n == nil
 			(1...Infinity).times do |i|
 				p i
 			end
 		else 
 			n.times do 
 				p2each do |e|
 					yield(e)
 				end
 			end
 		end
 		return
 	end

 	#define detect
    def p2detect(&blk)
        p2each do |e|
            if yield(e)
                return e
            end
        end
        return nil   
    end


	#define drop
 	def p2drop(n)
 		r = Array.new
 		p2each do |i|
 			r << i
 		end
 		a = Array.new
 		self.size.times do |num|
 			#p num
 			if num >= n
 				a << r[num]		
 			end			 
 		end	
 		#p a
 		return a
 	end

 	#define drop_while
 	def p2drop_while

 		a = Array.new
 		num = 0
 		p2each do |e|
 			if !yield(e)
 				a << e
 			end
 		end
 		#p a
 		return a
 	end

 	#define each_cons
 	def p2each_cons(n)
 		a = Array.new
 		p2each do |i|
 			a << i
 		end

 		((self.size)-n+1).times do |e|
 			b = Array.new
 			n.times do |j|
 				b << a[e+j]
 			end
 			yield(b)
 		end	
        return nil
 	end


 	#define each_slice
 	def p2each_slice(n)
 		a = Array.new
 		p2each do |i|
 			a << i
 		end

 		num = 0
 		e = 0
 		((self.size)/n).times do	
 			b = Array.new
 			n.times do |j|
 				num += 1
 				b << a[e + j]
 			end
 			#p b
 			e = e + n
 			yield(b)
 		end

 		c = Array.new
 		((self.size)%n).times do|p|
 			c << a[num + p]
 		end
 		#p c
 		yield(c)
 			
 	end


 	#define each_with_index
    def p2each_with_index
        i = 0
        p2each do |e|
            yield(e,i)
            i += 1
        end

    end


    #define entries
    def p2entries
        r = Array.new
        p2each do |e|
            #p e
            r << e
        end
        return r
    end

    #def find same as detect
    def p2find(&blk)
        return self.p2detect(&blk)
    end


    #def find_all
    def p2find_all
        r = Array.new
        p2each do |e|
            if yield(e)
                r << e
            end

        end  
        return r     
    end


	#def find_index
    def p2find_index
        i = 0
        p2each do |e|
            #p e
            if yield(e)
                return i
            end
            i += 1
        end
        return nil
    end

	#def first
    def p2first(n)
        a = Array.new

        i = 0
        p2each do |e|
            if i == n
                #p a
                return a
            else
                a << e
                i += 1
            end
        end           
    end

    #def group_by
    def p2group_by
        h = {}

        p2each do |e|
            key = yield(e)
            #p e
            #p key
            if h[key] != nil
                h[key] << e
            else
                h[key] = [e] 
            end
        end 
        return h     
    end

	#def inject()
    def p2inject(n)
        r = n
        p2each do | e |
            r = yield( r, e )
        end
        return r     
    end

    #def inject
    def para0inject
        first = true
        r = nil
        self.each do | e |
            if first
                r  = e
                first = false
            else
                r = yield( r, e )
            end
        end
        return r
    end

    #def minmax
    def p2minmax

        min = 0
        max = 0
        first = true
        p2each do |e|
            if first
                min = e
                max = e
                first = false
            else
                #p e
                #p min
                #p yield(e, min)
                if yield(e,min) == -1 
                    min = e
                    max = min
                elsif yield(e,min) == 1
                    max = e             
                end
            end
        end
        #p [min,max]
        return [min,max]
        
    end

    #def minmax_by
    def p2minmax_by
        min = 0
        max = 0
        first = true
        p2each do |e|
            if first
                min = e
                max = e
                first = false
            else
                if yield(e) > yield(max)
                    max = e
                end
                if yield(e) < yield(min)
                    min = e     
                end
            end  
        end
        return [min, max] 
    end


    #def partition
    def p2partition
        a = []
        b = []
        p2each do |e|
            if yield(e)
                a << e
            else
                b << e
            end
        end
        return [a, b]

    end

	#def reject
    def p2reject
        a = []
        p2each do |e|
            if !yield(e)
                a << e     
            end
        end
        return a    
    end


	#def take(n)
    def p2take(n)
        a = []
        num = 0
        p2each do |e|
            if num == n
                return a
            else
                a << e
                num += 1 
            end
        end

    end

    #def take_while
    def p2take_while
        a = []
        p2each do |e|
            #p e
            if !yield(e) 

                return a
            else
                a << e
            end
        end       
    end

    #def to_a
    def p2to_a
        a = []
        p2each do |e|
            a << e 
        end
        return a
    end


	#def to_h
    def p2to_h
    	a = {}
    	num = 0
    	p2each do|e|
    		#p e
    		a[e] = num
    		num += 1
    	end
    	#p a
    	return a  	
    end

end

class Array
    include P2Enumerable
    alias p2each each
end

class Hash
    include P2Enumerable
    alias p2each each
end

class Range
    include P2Enumerable
    alias p2each each
end

class Enumerator
    include P2Enumerable
    alias p2each each
end

class P2Tree
    include P2Enumerable
    #include Enumberale to implete each_with_index at the test_p2to_h
    include Enumerable  

end