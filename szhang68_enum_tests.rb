#test file
load("szhang68_enum.rb")


#test p2all?

def test_p2all?
    s = [1,2,3].p2all? {|num| num > 3}
    raise "#{__method__} array error!" if s
    p "#{__method__} array passed!"

    s = {1=>0, 2=>1, 3=>2, 4=>3}.p2all? {|r| r[0] == 1}
    raise "#{__method__} Hash error!" if s == false
    p "#{__method__} Hash passed!"
end
test_p2all?

#test p2any?
def test_p2any?
	s = ["hot", "water","stupid"].p2any? {|str| str.length > 10}
	raise "#{__method__} array error!" if s
	p "#{__method__} array passed!"

	s = {"hot"=>0, "water"=>1,"stupid"=>2}.p2any? {|h| h[0].length == 3}
	raise "#{__method__} Hash error!" if s
	p "#{__method__} Hash passed!"

end
test_p2any?

#test p2collect
def test_p2collect
	s = [2,4,6].p2collect {|n| [n, n*2]}
	raise "#{__method__} array error!" if s[0] != [2,4]
	p "#{__method__} array passed!"
	
	s = {"hot"=>0, "water"=>1,"stupid"=>2}.p2collect {"hhh"}
	raise "#{__method__} Hash error!" if s[0].length != 3
	p "#{__method__} Hash passed!"
end
test_p2collect


#test p2collect_concat
def test_p2collect_concat
	s = [2,4,6].p2collect_concat {|n| [n, n*2]}
	raise "#{__method__} array error!" if s[1] != 4
	p "#{__method__} array passed!"
	
	s = {"hot"=>0, "water"=>1,"stupid"=>2}.p2collect_concat {"hhh"}
	raise "#{__method__} Hash error!" if s[1] != "hhh"
	p "#{__method__} Hash passed!"
	
end
test_p2collect_concat


#test p2count
def test_p2count
	s = [1...10].p2count{|e| e == 4}
	raise "#{__method__} array error!" if !s
	p "#{__method__} array passed!"
	
	s = {"hot"=>0, "water"=>1,"stupid"=>2}.p2count{|e| e[1] == 0}
	raise "#{__method__} Hash error!" if !s
	p "#{__method__} Hash passed!"
    
end
test_p2count

#test p2cycle
def test_p2cycle
	r = []
	[1,2].p2cycle(2){|e| r << e}
	#p r
	raise "#{__method__} array error!" if r.length != 4
	p "#{__method__} array passed!"

	n = []
	{"hot"=>0, "water"=>1,"stupid"=>2}.p2cycle(2){|e| n << e}
	raise "#{__method__} Hash error!" if n.length != 6
	p "#{__method__} Hash passed!"
    
end
test_p2cycle


#test p2detect
def test_p2detect
	r = [2,3,4,5,6].p2detect{|e| e % 2 == 0} 
	raise "#{__method__} array error!" if r != 2
	p "#{__method__} array passed!"

	r = {"test" => 0, "detect" => 1, "method" => 2}.p2detect{|e| e[1] == 3}
	raise "#{__method__} Hash error!" if r != nil
	p "#{__method__} Hash passed!"
	
end
test_p2detect


#test p2drop
def test_p2drop
	r = [1,2,3,4,5,6].p2drop(2)
	raise "#{__method__} array error!" if r != [3,4,5,6]
	p "#{__method__} array passed!"

	n = {"hot"=>0, "water"=>1,"stupid"=>2}.p2drop(1)
	raise "#{__method__} Hash error!" if n.size != 2
	p "#{__method__} Hash passed!"
	
end
test_p2drop


#test drop_while
def test_p2drop_while
	r = [1,2,3,4,5,6].p2drop_while{|e| e > 4}
	raise "#{__method__} array error!" if r != [1,2,3,4]
	p "#{__method__} array passed!"

	n = {"hot"=>0, "water"=>1,"stupid"=>2}.p2drop_while{|e| e[1] != 0}
	raise "#{__method__} Hash error!" if n == ["hot", 0]
	p "#{__method__} Hash passed!"

end
test_p2drop_while


#test p2each_cons
def test_p2each_cons
	r = []
	(3..6).p2each_cons(2){|e| r << e}
	raise "#{__method__} array error!" if r != [[3,4],[4,5],[5,6]]
	p "#{__method__} array passed!"

	n = {"hot"=>0, "water"=>1,"cold"=>2, "rice"=>3}
	m = []
	n.p2each_cons(3){|e| m << e}
	#p m
	raise "#{__method__} Hash error!" if m != [[["hot", 0], ["water", 1], ["cold", 2]],[["water", 1], ["cold", 2], ["rice", 3]]]
	p "#{__method__} Hash passed!"
end
test_p2each_cons


#test p2each_slice
def test_p2each_slice
	r = []
	(3..7).p2each_slice(2){|e| r << e}
	raise "#{__method__} array error!" if r != [[3,4],[5,6],[7]]
	p "#{__method__} array passed!"

	n = {"hot"=>0, "water"=>1,"cold"=>2, "rice"=>3}
	m = []
	n.p2each_slice(3){|e| m << e}
	raise "#{__method__} Hash error!" if m != [[["hot", 0], ["water", 1], ["cold", 2]],[["rice", 3]]]
	p "#{__method__} Hash passed!"
	
end
test_p2each_slice


#test p2each_with_index
def test_p2each_with_index
	hash = Hash.new
	["each", "with","index"].p2each_with_index{|item, index| hash[item] = index} 
	#p hash
	raise "#{__method__} array error!" if hash != {"each" => 0, "with" => 1, "index" => 2}
	p "#{__method__} array passed!"

	#hash test
	n = {"hot"=>0, "water"=>1,"cold"=>2, "rice"=>3}
	item = Array.new
	index = Array.new
	n.each_with_index do |e,i|
		item << e 
		index << i
	end
	raise "#{__method__} Hash error!" if item != [["hot", 0], ["water", 1], ["cold", 2], ["rice", 3]] || index != [0, 1, 2, 3]
	p "#{__method__} Hash passed!"
end
test_p2each_with_index

#test p2entries
def test_p2entries
	r = ["this","is","a","test"].p2entries
	raise "#{__method__} array error!" if r != ["this", "is", "a", "test"]
	p "#{__method__} array passed!"

	r = {"tomorrow" => 0, "is" => 1, "another" => 2, "day" => 3}.p2entries
	raise "#{__method__} Hash error!" if r != [["tomorrow", 0], ["is", 1], ["another", 2], ["day", 3]]
	p "#{__method__} Hash passed!"
	
end
test_p2entries


#test p2find
def test_p2find
	r = [3,4,52,3,5].p2find{|e| e == 9}
	p r
	raise "#{__method__} array error!" if r!= nil
	p "#{__method__} array passed!"

	r = {"test" => 0, "detect" => 1, "method" => 2}.p2find{|e| e[0] == "test"} 
	raise "#{__method__} Hash error!" if r != ["test", 0]
	p "#{__method__} Hash passed!"
	
end
test_p2find


#test p2find_all
def test_p2find_all
	r = [3,4,55,3,5,15,4,20].p2find_all{|e| e % 5 == 0}
	raise "#{__method__} array error!" if r != [55,5,15,20]
	p "#{__method__} array passed!"

	r = {"test" => 0, "find_all" => 1, "method" => 2}.p2find_all{|e| e[1] == 1 || e[1] == 0} 
	raise "#{__method__} Hash error!" if r != [["test",0],["find_all",1]]
	p "#{__method__} Hash passed!"
	
end
test_p2find_all


#test p2find_index
def test_p2find_index
	r = [2,9,4,6,8,12,35,43].p2find_index{|e| e%3 == 0}
	#p r
	raise "#{__method__} array error!" if r != 1
	p "#{__method__} array passed!"

	r = {"test" => 0, "find" => 1, "index" => 2, "method" => 3}.p2find_index{|e| e[1] == 2}
	#p r
	raise "#{__method__} Hash error!" if r != 2
	p "#{__method__} Hash passed!"

end
test_p2find_index

#test p2first
def test_p2first
	r = %w[test first method].p2first(2)
	raise "#{__method__} array error!" if r != ["test", "first"]
	p "#{__method__} array passed!"

	r = {"test" => 0, "find" => 1, "index" => 2, "method" => 3}.p2first(1)
	raise "#{__method__} Hash error!" if r!= [["test",0]]
	p "#{__method__} hash passed!"

end
test_p2first

#test p2group_by
def test_p2group_by
	r = (1..8).p2group_by{|i| i%5}
	raise "#{__method__} array error!"if r != {1=>[1, 6], 2=>[2, 7], 3=>[3, 8], 4=>[4], 0=>[5]}
	p "#{__method__} array passed!"

	r = {"test" => 0, "group" => 1, "by" => 2, "method" => 3}.p2group_by{|e| e[1]%2}
	raise "#{__method__} Hash error!" if r != {0=>[["test", 0], ["by", 2]], 1=>[["group", 1], ["method", 3]]}
	p "#{__method__} Hash passed!"

end
test_p2group_by


#test p2inject()
def test_p2inject
	#r = (2..6).p2inject{|s,e| s+e}
	r = (2..6).p2inject(1){|s,e| s*e}
	
	raise "#{__method__} array error!"if r != 720 
	p "#{__method__} array passed!"

	r = {"test" => 0, "inject" => 1,  "method" => 2}
	m = r.p2inject([]){|s,e| s += e}
	

	raise "#{__method__} Hash error!"if m!= ["test", 0, "inject", 1, "method", 2] 
		p "#{__method__} Hash passed!"
end
test_p2inject

#test para0inject
def test_para0inject
	m = (2..6).para0inject{|s,e| s+e}
	raise "#{__method__} array error!"if m != 20
	p "#{__method__} array passed!"

	r = {"test" => 0, "inject" => 1,  "method" => 2}
	n = r.para0inject{|s,e| s << e}
	raise "#{__method__} Hash error!"if n != ["test", 0, ["inject", 1], ["method", 2]]
	p "#{__method__} Hash passed!"
	
end
test_para0inject



#test p2minmax
def test_p2minmax
	a = %w(a aaaa b bb acd dsafdea)
	r = a.p2minmax{|a, b| a.length <=> b.length}
	raise "#{__method__} array error!" if r != ["a", "dsafdea"]
	p "#{__method__} array passed!"

	a = {"test" => 0, "minmax" => 1,  "function" => 2}
	r = a.p2minmax{|a, b| a[0].length <=> b[0].length}
	raise "#{__method__} Hash error!" if r != [["test", 0], ["function", 2]]
	p "#{__method__} Hash passed!"

end
test_p2minmax

#test p2minmax_by
def test_p2minmax_by

	a = %w(let us test ha hahaha pei)
	r = a.p2minmax_by{|a| a.length}	
	raise "#{__method__} array error!" if r!= ["us", "hahaha"]
	p "#{__method__} array passed!"

	a = {"test" => 0, "minmax" => 1,  "by" => 2, "function" => 3}
	r = a.p2minmax_by{|a| a[0].length}
	raise "#{__method__} Hash error!" if r != [["by", 2], ["function", 3]]
	p "#{__method__} Hash passed!"
	
end
test_p2minmax_by

#test p2partition
def test_p2partition
	r = (1..10).p2partition{|e| e.odd?}
	raise "#{__method__} array error!" if r!= [[1, 3, 5, 7, 9], [2, 4, 6, 8, 10]]
	p "#{__method__} array passed!"

	a = {"test" => 3, "partition" => 1,  "kk" => 5}
	r = a.p2partition{|e| e[1].even?}
	raise "#{__method__} array error!" if r!= [[], [["test", 3], ["partition", 1], ["kk", 5]]]
	p "#{__method__} array passed!"
	
end
test_p2partition


#test p2reject
def test_p2reject
	r = (1..10).p2reject{|e| e%3 != 0}
	raise "#{__method__} array error!" if r!= [3, 6, 9]
	p "#{__method__} array passed!"

	a = {"test" => 3, "reject" => 1,  "kkkk" => "hhh", "jjjj" => "diff"}
	r = a.p2reject{|e| e[0].length == 4}
	raise "#{__method__} Hash error!" if r!= [["reject", 1]]
	p "#{__method__} Hash passed!"

	
end
test_p2reject

#test p2take
def test_p2take
	r = %w(a aa aaa aaaa h hh hhhh).p2take(8)
	raise "#{__method__} array error!" if r!= ["a", "aa", "aaa", "aaaa", "h", "hh", "hhhh"]
	p "#{__method__} array passed!"

	a = {"test" => "", "take" => 1,  "n" => "hhh", "jjjj" => "diff"}
	r = a.p2take(2)
	raise "#{__method__} Hash error!" if r!= [["test", ""], ["take", 1]]
	p "#{__method__} Hash passed!"

end
test_p2take

#test p2take_while
def test_p2take_while
	r = [2,3,56,6,3,6,8,9].p2take_while{|e| e < 10}
	raise "#{__method__} array error!" if r!= [2, 3]
	p "#{__method__} array passed!"

	a = {"test" => "e", "take" => 34,  "n" => 1, "jjjj" => "diff"}
	r = a.p2take_while{|e| e[1] != 1}
	raise "#{__method__} Hash error!" if r!= [["test", "e"], ["take", 34]]
	p "#{__method__} Hash passed!"
	
end
test_p2take_while

#test p2to_a
def test_p2to_a
	r = [1,2,4,52,6,3].p2to_a
	raise "#{__method__} array error!" if r!= [1, 2, 4, 52, 6, 3]
	p "#{__method__} array passed!"

	r = {"test" => "e", "take" => 34,  "n" => 1, "jjjj" => 6}.p2to_a
	raise "#{__method__} Hash error!" if r!= [["test", "e"], ["take", 34], ["n", 1], ["jjjj", 6]]
	p "#{__method__} Hash passed!"
	
end
test_p2to_a



#test p2to_h
def test_p2to_h
	r = %w(er ja da 45 ev).each_with_index.p2to_h
	raise "#{__method__} array error!" if r!= {"er"=>0, "ja"=>1, "da"=>2, "45"=>3, "ev"=>4}
	p "#{__method__} array passed!"
	
	r = {"test" => "e", "take" => 6}.each_with_index.p2to_h
	raise "#{__method__} Hash error!" if r!= {["test", "e"]=>0, ["take", 6]=>1}
	p "#{__method__} Hash passed!"
	
end
test_p2to_h




