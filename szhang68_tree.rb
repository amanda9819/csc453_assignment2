class P2Tree
	def initialize
		@tree = Array.new
		@nodeNum = 0
		@maxDepth = 20

	end

	#insertNode
	def insertNode(a)
		if @nodeNum == 0 
			@tree[@nodeNum] = a
			@nodeNum += 1
			return 
			
		else
			@tree.size.times do |i|
				#p i
				#p a
				if @tree[i] != nil
					if (a > @tree[i]) && (@tree[2*i + 1] == nil)
						@tree[2*i + 1] = a
						@nodeNum += 1
						#p @tree
						return 0
					elsif (a < @tree[i]) && (@tree[2*i + 2] == nil)
						@tree[2*i + 2] = a
						@nodeNum += 1
						#p @tree
						return 0
					end
				end
				
			end

		end

	end

	def p2each(&blk)
		r = Array.new
		@tree.size.times do |i|
			if @tree[i] != nil
				yield(@tree[i])
			end
		end
		
	end

	def each(&blk)
		self.p2each(&blk)
	end

	def p2each_with_level
		r = []
		@tree.size.times do |i|
			if @tree[i] != nil
				@maxDepth.times do |j|
					if (2 ** j - 1 <= i) && (i <= 2 ** (j+1) - 2)
						#p @tree[i]
						#p j
						yield(@tree[i],j)
					end
				end		
			end
		end
	end

	def size
		return @nodeNum
	end
	
end